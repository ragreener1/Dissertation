\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}

\title{First Progress Report}
\author{Robert Greener}
\usepackage[nolist,nohyperlinks]{acronym}
\usepackage{lmodern}
\usepackage{pdflscape}
\usepackage{graphicx}
\usepackage[style=ieee, maxcitenames=15]{biblatex}

\DeclareBibliographyCategory{inbib}

\makeatletter
\AtEveryCitekey{%
  \ifcsstring{blx@delimcontext}{fullcite}
    {}
    {\addtocategory{inbib}{\thefield{entrykey}}}}
\makeatother

\addbibresource{library.bib}

% acronyms
\acrodef{ABM}[ABM]{\emph{agent-based model}}
\acrodef{GUI}[GUI]{\emph{graphical user interface}}
\acrodef{IA}[IA]{\emph{intervention agent}}

\DeclareFieldFormat{urldate}{[Accessed \thefield{urlday}/\thefield{urlmonth}/\thefield{urlyear}\isdot]}

\begin{document}

\maketitle

\section{Project Motivation}
Exercise is widely accepted to have a positive effect on cardiovascular health, with active commuting that includes walking and cycling resulting in an 11\% reduction in cardiovascular risk~\cite{Hamer2008}. This has been recognised by government, with the Department of Health noting that ``active travel and physical activity need to become the norm in communities''~\cite{DoH2010}. Targeted interventions can be used to encourage a modal shift from inactive transport, to more active methods, however interventions are often expensive, and there is limited research on the effectiveness of these~\cite{Aldred2018}.

The use of \acp{ABM} to model transport, could allow for an inexpensive way for interventions to be tested prior to implementation, to gauge their potential effect. In particular, by modelling the social norms surrounding commuting and active travel, including how these concepts spread, and how they can be manipulated, it may be possible to test novel low-cost interventions, that could have a greater effect, due to social influence.

\section{Aims and Objectives}
The aim is to build on a pre-existing \ac{ABM} - MOTIVATE\footnote{https://github.com/ragreener1/Motivate-legacy}, extending it in four ways:
\begin{enumerate}
	\item Add more useful output to the model: currently the output of the model is a CSV file of the run. More concise and meaningful output would be useful, as it would allow users to quickly evaluate different interventions.
	\item Replace the current model of social interaction, with an influence propagation model from the literature. This should create a model that is more realistic, and focusses on the psychology of the agents.
	\item Implement a wide range of interventions, that allow the user to focus on the physical barriers to active commuting, as well as the social norms surrounding commuting, and how a normative change may be brought about. These interventions should be fully customisable, allowing for a combination of interventions, of different costs, sizes and time periods.
	\item Convert the current model from a Rust application into a library with Python bindings, this is useful for two reasons:
	\begin{enumerate}
		\item A simple and straightforward \ac{GUI} can be created using Python and GTK, that will allow non-technical users to configure and run the simulation, and present them with the output in a useful manner.
		\item It will allow the model to be easily scripted by more technical users, allowing them to run a large number of simulations on high performance computing platforms.
	\end{enumerate}
	
\end{enumerate}
Together, this should allow a non-technical user to evaluate different interventions, and combinations of interventions, that could take place in an urban environment.

\section{Existing Literature}
\begin{itemize}

	\item \textbf{\fullcite{Archbold2018}}. This paper presents a method by which the spread of a concept can be maximised or minimised, by using other concepts that either conflict or complement the one you are attempting to manipulate.  This is particularly useful, as certain concepts, such as the desirability of owning a car, may wish to be minimised, while others, such as importance of physical activity, may wish to be maximised. By using the techniques in this paper, more realistic social interaction can be modelled, which can then be used for interventions which attempt to manipulate the spread of concepts, to cause a modal shift in transport methods.

	\item \textbf{\fullcite{Marchant2015}}. This introduces \acp{IA}, which are fixed strategy agents. They can be thought of as being ``equivalent to incentivising individuals to take particular actions, for example through reward or payment''. The use of \acp{IA} could be one type of intervention that can be simulated using the model, for example giving financial reward for active commuting, or alternatively removing the ability for inactive travel for some agents, which could be achieved by car-free days.

	\item \textbf{\fullcite{Marchant2014}}. This uses \acp{IA} to ``examine the minimum level of intervention required to cause destabilisation, and explore the effect of different pricing mechanisms on the cost of interventions''.  The cost of interventions will be captured in the model, as such understanding and being to evaluate the cost of \acp{IA}, where they may have upfront {and/or} ongoing costs, will be an important part of the model.

	\item \textbf{\fullcite{Mueller2013}}. This ``establishes a standard for describing \acp{ABM} that includes human decision-making ({ODD+D})''. As such this is incredibly useful for the specification and design of the model, as {ODD+D} gives a clear method by which to describe the model.

	\item \textbf{\fullcite{Aldred2018}}. This is an evaluation of the Mini-Holland active travel intervention in London. The model is loosely based on Waltham Forest, one of the London boroughs where the Mini-Holland programme was introduced. As such, this is useful for validating the model, as the model can be run with a similar intervention, and the results compared to the real-world results.
	
	\item \textbf{\fullcite{Davidsson2005}}. ``This paper provides a survey of existing research on agent-based approaches to transportation and traffic management''. This provides some background for the model, and techniques used in earlier research could be adapted for use in the model.

\end{itemize}

\section{Software Platform}
This project is an extension of a pre-existing \ac{ABM}, which was developed in Rust, as such the majority of the code will be implemented in Rust. However, a \ac{GUI} will be implemented using Python~3, using PyGObject bindings for GTK+ version 3, with the user interface designed in Glade, a \ac{GUI} builder for GTK+. This will allow configuration through a user interface, making the model accessible for non-technical users. Implementing the \ac{GUI} will involve rewriting the current Rust application as a library, and then writing Python~3 bindings for this. 

All of the programming languages, and packages to be used are cross-platform, however Microsoft Windows 10 will be the target operating system, as it should be possible for policy makers to use the software to evaluate interventions, who will most likely be using Windows.

\section{Plan of Progress}

\begin{landscape}
\subsection{Gantt chart}


\includegraphics[width=1.7\textheight]{ganttOct.png}


\noindent \includegraphics[width=1.7\textheight]{ganttNov.png}


\noindent \includegraphics[width=1.7\textheight]{ganttDec.png}


\noindent \includegraphics[width=1.7\textheight]{ganttJan.png}


\noindent \includegraphics[width=1.7\textheight]{ganttFeb.png}


\noindent \includegraphics[width=1.7\textheight]{ganttMar.png}


\noindent \includegraphics[width=1.7\textheight]{ganttApr.png}
\end{landscape}

\subsection{Deliverables}
\begin{itemize}
	\item The extension to the MOTIVATE model implemented in Rust.
	\item A GUI for the model implemented in Python~3, using GTK+~3.
\end{itemize}

\medskip

\printbibliography[category=inbib]

\end{document}
